var mymap;
var satellite;
var osm;
var basemaps;
var drawnItems;

mychart = $('#mychart');
//Chart.defaults.global.defaultFontFamily ='Lato',
Chart.defaults.global.defaultFontColor ='black',
    chart_in = new Chart(mychart, {
        type: 'horizontalBar',
        data: {
            labels:['Hospitals', 'Schools', 'Stores', 'Restaurants'],
            datasets:[{
                label:'Number',
                data:[0,0,0,0],
                backgroundColor:['red','yellow','grey','blue'],
                borderWidth:1,
                borderColor:'black',
                hoverBorderWidth:4,
                hoverBorderColor:'green'
            }]

        },
        options:{
            maintainAspectRatio:false,
            responsive:true,
            title:{
                display:false,
                text:'Amenities',
            },
            legend:{
                position:'bottom',
                labels: {
                    fontcolor:'red',
                }
            },
            layout:{
                padding:{
                    left:0,
                    right:0,
                    top:0,
                    bottom:0
                }
            },
            tooltips:{
                enabled:true,
            }
        }
    }) 

function cr_popup(tags, id) {
    let row;
    const link = document.createElement('a');
    const table = document.createElement('table');
    const div = document.createElement('div');

    table.style.borderSpacing = '10px';
    table.style.borderCollapse = 'separate';

    for (const key in tags) {
      row = table.insertRow(0);
      row.insertCell(0).appendChild(document.createTextNode(key));
      row.insertCell(1).appendChild(document.createTextNode(tags[key]));
    }

    div.appendChild(link);
    div.appendChild(table);

    return div;
  };


$(document).ready(function(){
    mymap = L.map('map_id',{
        center:[18.74647, 83.401877],
        zoom:5,
        minZoom:2,
        maxZoom: 19,
        layersControl:true,
        //drawControl: true,
    });

    

    satellite = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 21,
        id: 'mapbox.satellite',
        accessToken: 'sk.eyJ1IjoiaGFyc2hhNjc3MiIsImEiOiJja2o0bDY4MDQwbHgwMnpvOWYyNmtvcGpxIn0.pkPjrHsegZ8wuHMpeJKRvg'
    }).addTo(mymap);

    osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(mymap) //same as "mymap.addLayer(osm)"

    basemaps = {
        "Satellite" : satellite,
        "OSM":osm,
        
    };

    
    drawnItems  = L.featureGroup().addTo(mymap);
    //L.drawLocal.draw.handlers.rectangle.tooltip.start = 'Please draw an area less than 1000 hectares';

    var drawControlFull = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems,
        },

        draw: {
            polyline:false,
            marker :true,
            circle:false,
            polygon : false,
            rectangle:false,
            
        }
    });

    var drawControlEditOnly = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems
        },
        draw: false
    });


    mymap.addControl(drawControlFull);

    L.control.layers(basemaps,{'drawLayer':drawnItems}).addTo(mymap);

    var jsonObj
    mymap.on('draw:created', function(e){
        var layer = e.layer;
        drawnItems.addLayer(layer);
        //console.log(layer.getLatLng().lng);
        $('#user_lat').val(layer.getLatLng().lat);
        $('#user_lon').val(layer.getLatLng().lng);
        //geoJSON_obj = layer.toGeoJSON();
        //jsonObj = JSON.stringify(geoJSON_obj);
        //$('#user_aoi').val(jsonObj);
        drawControlFull.remove(mymap);
        drawControlEditOnly.addTo(mymap);
    });


    mymap.on('draw:deleted', function(e) {
        $('#user_lat').val('');
        $('#user_lon').val('');
        drawControlEditOnly.remove(mymap);
        drawControlFull.addTo(mymap);
     });
     
    
        $('#city_id').change(function(){
            //var a1 = $('#city_id option:selected').value;
    
            if ($('#city_id option:selected').val()=='Hyderabad'){
                mymap.setView([17.407,78.497],11);
            }
    
            else if ($('#city_id option:selected').val()=='Banglore'){
                mymap.setView([12.985,77.592],12);
            }
    
            else if ($('#city_id option:selected').val()=='Chennai'){
                mymap.setView([13.082,80.238],12);
            }

            else if ($('#city_id option:selected').val()=='Mumbai'){
                mymap.setView([19.076,72.877],12);
            }

            else if ($('#city_id option:selected').val()=='Delhi'){
                mymap.setView([28.648,77.225],11);
            }

            else if ($('#city_id option:selected').val()=='Kolkata'){
                mymap.setView([22.572,88.363],12);
            }
        })


        var s_i = L.icon({
            iconUrl: '/static/icons/school3.png',
            iconSize:     [40]
        });
        var p_i = L.icon({
            iconUrl: '/static/icons/park.png',
            iconSize:     [30]
        });
        var h_i = L.icon({
            iconUrl: '/static/icons/hospital3.png',
            iconSize:     [30]
        });
        var b_i = L.icon({
            iconUrl: '/static/icons/bus.png',
            iconSize:     [30]
        });
        var r_i = L.icon({
            iconUrl: '/static/icons/restaurant.png',
            iconSize:     [30]
        });
        var g_i = L.icon({
            iconUrl: '/static/icons/grocery2.png',
            iconSize:     [40]
        });

        $('#sub_id').click(function(){
            var lat = $('#user_lat').val();
            var lon = $('#user_lon').val();
            var rad = $('#radius_id').val();

            $("#dummy_id").css("display", "block");

            //var my_loc = L.marker([17.407,78.497],{icon:r_i}).addTo(mymap).bindPopup("your location")
        
            //var my_loc = L.marker([lat,lon]).addTo(mymap).bindPopup("your location")

            var my_aoi = L.circle([lat,lon],{radius:rad, color:'black',fillOpacity:0.1}).addTo(mymap);
            mymap.setView([lat,lon],15);
                        
        })

        $('#r_id').click(function(){

            $("#r_id").attr("disabled", true);
            $("#mychart").css("display", "block");

            var lat = $('#user_lat').val();
            var lon = $('#user_lon').val();
            var rad = $('#radius_id').val();
            var opl_r = new L.OverPassLayer({

                query: 'node["amenity"~"restaurant|cafe|fast_food|food_court|cafe|ice_cream"](around:' 
                + rad.toString()+ ',' + lat.toString()+ ',' + lon.toString()+ '); out;',
    
                markerIcon : r_i,
    
                minZoom: 14,
    
                onSuccess : function (data){
                    //var res = $('#r_n').val(data.elements.length);
                    //$("#r_n").css("display", "block");
                    for (let i = 0; i < data.elements.length; i++) {
                        let pos;
                        let marker;
                        const e = data.elements[i];
                        if (e.type === 'node') {
                            pos = L.latLng(e.lat, e.lon);
                          } else {
                            pos = L.latLng(e.center.lat, e.center.lon);
                          }
                        marker = L.marker(pos, { icon: r_i });
                        const popupContent = cr_popup(e.tags, e.id)
                        const popup = L.popup().setContent(popupContent);
                        marker.bindPopup(popup);
                        mymap.addLayer(marker);
    
    
                    }
                    chart_in.data.datasets[0].data[3] = data.elements.length;
                    chart_in.update()
                }
            }).addTo(mymap);

        })


        $('#s_id').click(function(){

            $("#s_id").attr("disabled", true);
            $("#mychart").css("display", "block");

            var lat = $('#user_lat').val();
            var lon = $('#user_lon').val();
            var rad = $('#radius_id').val();
            var opl_r = new L.OverPassLayer({

                query: 'node["amenity"~"college|school|kindergarten"](around:' 
                + rad.toString()+ ',' + lat.toString()+ ',' + lon.toString()+ '); out;',
    
                markerIcon : r_i,
    
                minZoom: 14,
    
                onSuccess : function (data){
                    //var sch = $('#s_n').val(data.elements.length);
                    //$("#s_n").css("display", "block");
                    for (let i = 0; i < data.elements.length; i++) {
                        let pos;
                        let marker;
                        const e = data.elements[i];
                        if (e.type === 'node') {
                            pos = L.latLng(e.lat, e.lon);
                          } else {
                            pos = L.latLng(e.center.lat, e.center.lon);
                          }
                        marker = L.marker(pos, { icon: s_i });
                        const popupContent = cr_popup(e.tags, e.id)
                        const popup = L.popup().setContent(popupContent);
                        marker.bindPopup(popup);
                        mymap.addLayer(marker);
    
    
                    }
                    chart_in.data.datasets[0].data[1] = data.elements.length;
                    chart_in.update()
                }
            }).addTo(mymap);

        })

        $('#h_id').click(function(){

            $("#h_id").attr("disabled", true);
            $("#mychart").css("display", "block");

            var lat = $('#user_lat').val();
            var lon = $('#user_lon').val();
            var rad = $('#radius_id').val();
            var opl_r = new L.OverPassLayer({

                query: 'node["amenity"~"clinic|hospital|pharmacy|dentist"](around:' 
                + rad.toString()+ ',' + lat.toString()+ ',' + lon.toString()+ '); out;',
    
                markerIcon : r_i,
    
                minZoom: 14,
    
                onSuccess : function (data){
                    //var hos = $('#h_n').val(data.elements.length);
                    //$("#h_n").css("display", "block");
                    for (let i = 0; i < data.elements.length; i++) {
                        let pos;
                        let marker;
                        const e = data.elements[i];
                        if (e.type === 'node') {
                            pos = L.latLng(e.lat, e.lon);
                          } else {
                            pos = L.latLng(e.center.lat, e.center.lon);
                          }
                        marker = L.marker(pos, { icon: h_i });
                        const popupContent = cr_popup(e.tags, e.id)
                        const popup = L.popup().setContent(popupContent);
                        marker.bindPopup(popup);
                        mymap.addLayer(marker);
    
    
                    }
                    chart_in.data.datasets[0].data[0] = data.elements.length;
                    chart_in.update()
                }
            }).addTo(mymap);

        })


        $('#g_id').click(function(){

            $("#g_id").attr("disabled", true);
            $("#mychart").css("display", "block");

            var lat = $('#user_lat').val();
            var lon = $('#user_lon').val();
            var rad = $('#radius_id').val();
            var opl_r = new L.OverPassLayer({

                query: 'node["shop"~"coffee|convenience|dairy|farm|greengrocer|spices|tea|department_store|general|kiosk|supermarket|wholesale"](around:' 
                + rad.toString()+ ',' + lat.toString()+ ',' + lon.toString()+ '); out;',
    
                markerIcon : r_i,
    
                minZoom: 14,
    
                onSuccess : function (data){
                    //var gyms = $('#g_n').val(data.elements.length);
                    //$("#g_n").css("display", "block");
                    for (let i = 0; i < data.elements.length; i++) {
                        let pos;
                        let marker;
                        const e = data.elements[i];
                        if (e.type === 'node') {
                            pos = L.latLng(e.lat, e.lon);
                          } else {
                            pos = L.latLng(e.center.lat, e.center.lon);
                          }
                        marker = L.marker(pos, { icon: g_i });
                        const popupContent = cr_popup(e.tags, e.id)
                        const popup = L.popup().setContent(popupContent);
                        marker.bindPopup(popup);
                        mymap.addLayer(marker);
    
    
                    }
                    chart_in.data.datasets[0].data[2] = data.elements.length;
                    chart_in.update()
                }
            }).addTo(mymap);

        })



        
            /*
            chart_in.data.datasets[0].data[4] = 16;
            chart_in.data.datasets[0].backgroundColor[4] = 'green';
            chart_in.data.labels[4] = 'random';
            chart_in.update()
            */
})