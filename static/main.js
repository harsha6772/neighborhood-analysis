var mymap;
var satellite;
var osm;
var basemaps;
var drawnItems;
$(document).ready(function(){
    mymap = L.map('map_id',{
        center:[18.74647, 83.401877],
        zoom:5,
        minZoom:2,
        maxZoom: 18,
        layersControl:true,
        //drawControl: true,
    });

    

    satellite = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 21,
        id: 'mapbox.satellite',
        accessToken: 'sk.eyJ1IjoiaGFyc2hhNjc3MiIsImEiOiJja2o0bDY4MDQwbHgwMnpvOWYyNmtvcGpxIn0.pkPjrHsegZ8wuHMpeJKRvg'
    }).addTo(mymap);

    osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(mymap) //same as "mymap.addLayer(osm)"

    basemaps = {
        "Satellite" : satellite,
        "OSM":osm,
        
    };

    
    drawnItems  = L.featureGroup().addTo(mymap);
    //L.drawLocal.draw.handlers.rectangle.tooltip.start = 'Please draw an area less than 1000 hectares';

    var drawControlFull = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems,
        },

        draw: {
            polyline:false,
            marker :true,
            circle:false,
            polygon : false,
          /*  {
                allowIntersection:true,
                showArea:true,
                tap:false,
                shapeOptions: {
                    stroke: false,
                    color: 'red',
                    weight: 7,
                    opacity: 0.5,
                    fill: true,
                    fillColor: 'yellow', //same as color by default
                    fillOpacity: 0.4,
                    clickable: true
                }
            }, */
            rectangle:false,
            
        }
    });

    var drawControlEditOnly = new L.Control.Draw({
        edit: {
            featureGroup: drawnItems
        },
        draw: false
    });


    mymap.addControl(drawControlFull);

    L.control.layers(basemaps,{'drawLayer':drawnItems}).addTo(mymap);

    var jsonObj
    mymap.on('draw:created', function(e){
        var layer = e.layer;
        drawnItems.addLayer(layer);
        //console.log(layer.getLatLng().lng);
        $('#user_lat').val(layer.getLatLng().lat);
        $('#user_lon').val(layer.getLatLng().lng);
        //geoJSON_obj = layer.toGeoJSON();
        //jsonObj = JSON.stringify(geoJSON_obj);
        //$('#user_aoi').val(jsonObj);
        drawControlFull.remove(mymap);
        drawControlEditOnly.addTo(mymap);
    });


    mymap.on('draw:deleted', function(e) {
        $('#user_lat').val('');
        $('#user_lon').val('');
        drawControlEditOnly.remove(mymap);
        drawControlFull.addTo(mymap);
     });
     
    
        $('#city_id').change(function(){
            //var a1 = $('#city_id option:selected').value;
    
            if ($('#city_id option:selected').val()=='Hyderabad'){
                mymap.setView([17.407,78.497],11);
            }
    
            else if ($('#city_id option:selected').val()=='Banglore'){
                mymap.setView([12.985,77.592],12);
            }
    
            else if ($('#city_id option:selected').val()=='Chennai'){
                mymap.setView([13.082,80.238],12);
            }

            else if ($('#city_id option:selected').val()=='Mumbai'){
                mymap.setView([19.076,72.877],12);
            }

            else if ($('#city_id option:selected').val()=='Delhi'){
                mymap.setView([28.648,77.225],11);
            }

            else if ($('#city_id option:selected').val()=='Kolkata'){
                mymap.setView([22.572,88.363],12);
            }
        })


        var s_i = L.icon({
            iconUrl: '/static/icons/school.png',
            iconSize:     [20]
        });
        var p_i = L.icon({
            iconUrl: '/static/icons/park.png',
            iconSize:     [20]
        });
        var h_i = L.icon({
            iconUrl: '/static/icons/hospital.png',
            iconSize:     [20]
        });
        var b_i = L.icon({
            iconUrl: '/static/icons/bus.png',
            iconSize:     [20]
        });
        var r_i = L.icon({
            iconUrl: '/static/icons/restaurant.png',
            iconSize:     [20]
        });
        var g_i = L.icon({
            iconUrl: '/static/icons/gym.png',
            iconSize:     [20]
        });

        $('#sub_id').click(function(){
            var lat = $('#user_lat').val();
            var lon = $('#user_lon').val();
            var rad = $('#radius_id').val();

        //var my_loc = L.marker([17.407,78.497],{icon:r_i}).addTo(mymap).bindPopup("your location")
            //var my_loc = L.marker([lat,lon]).addTo(mymap).bindPopup("your location")

        var opl_r = new L.OverPassLayer({

            query: 'nwr["amenity"~"restaurant|cafe|fast_food|food_court|cafe|ice_cream"](around:' 
            + rad.toString()+ ',' + lat.toString()+ ',' + lon.toString()+ '); out;',

            markerIcon : r_i,

            minZoom: 15,
        }).addTo(mymap);

            var my_aoi = L.circle([lat,lon],{radius:rad, color:'black',fillOpacity:0.1}).addTo(mymap);
            mymap.setView([lat,lon],15);
                        
        })
        
        /*
        var opl_s = new L.OverPassLayer({

            query: 'nwr["amenity"~"school|library|college|university"](around:' 
            + rad.toString()+ ',' + lat.toString()+ ',' + lon.toString()+ '); out;',

            markerIcon : s_i,

            minZoom: 15,
        }).addTo(mymap);

        var opl_b = new L.OverPassLayer({

            query: 'nwr["amenity"~"bus_station"](around:' 
            + rad.toString()+ ',' + lat.toString()+ ',' + lon.toString()+ '); out;',

            markerIcon : b_i,

            minZoom: 15,
        }).addTo(mymap);

        var opl_b = new L.OverPassLayer({

            query: 'nwr["amenity"~"hospital|clinic|dentist|pharmacy"](around:' 
            + rad.toString()+ ',' + lat.toString()+ ',' + lon.toString()+ '); out;',

            markerIcon : h_i,

            minZoom: 15,
        }).addTo(mymap);
        */

        /*
        var my_aoi = L.circle([17.407,78.497],{radius:1000, color:'black',fillOpacity:0.1}).addTo(mymap);

        //var my_loc = L.marker([17.407,78.497],{icon:r_i}).addTo(mymap).bindPopup("your location")
        var my_loc = L.marker([17.407,78.497]).addTo(mymap).bindPopup("your location")

        var opl = new L.OverPassLayer({

            query: 'nwr["amenity"~"restaurant|cafe|fast_food|food_court|cafe|bar|pub"](around:1000,17.407,78.497); out;',
            markerIcon : r_i,
            
           
        });
        mymap.addLayer(opl);
       */
    

})