var grid_style = {
    color:'black',
    opacity:1,
    weight:0.5,
    fillOpacity:0
}

var chart_options = {
    maintainAspectRatio:false,
    responsive:true,
    title:{
        display:true,
        text:'Crop Classes based on NDVI',
    },
    legend:{
        display:true,
        position:'bottom',
        labels: {
            fontcolor:'red',
        }
    },
    layout:{
        padding:{
            left:0,
            right:0,
            top:0,
            bottom:0
        }
    },
    tooltips:{
        enabled:true,
    }
}

var grid_obj = fetch('/media/indices/'+data1.grid_name)
        .then(function(resp){
            return resp.json();
        })
        .then(function(data1){
            var grid0 =  L.geoJSON(data1.features[0],{style: grid_style}).addTo(mymap);
            var grid1 =  L.geoJSON(data1.features[1],{style: grid_style}).addTo(mymap);
            var grid2 =  L.geoJSON(data1.features[2],{style: grid_style}).addTo(mymap);
            var grid3 =  L.geoJSON(data1.features[3],{style: grid_style}).addTo(mymap);
            var grid4 =  L.geoJSON(data1.features[4],{style: grid_style}).addTo(mymap);
            var grid5 =  L.geoJSON(data1.features[5],{style: grid_style}).addTo(mymap);
            var grid6 =  L.geoJSON(data1.features[6],{style: grid_style}).addTo(mymap);
            var grid7 =  L.geoJSON(data1.features[7],{style: grid_style}).addTo(mymap);
            var grid8 =  L.geoJSON(data1.features[8],{style: grid_style}).addTo(mymap);

            var b = new Array(9);
            var u = new Array(9);
            var m = new Array(9);
            var h = new Array(9);
            var pb = new Array(9);
            var pu = new Array(9);
            var pm = new Array(9);
            var ph = new Array(9);
            var tot= new Array(9);
            for (var j = 0; j<9; j++){
                b[j] =  data1.features[j].properties.bare_Soil;
                u[j] =  data1.features[j].properties.unhealthy_veg;
                m[j] =  data1.features[j].properties.moderate;
                h[j] =  data1.features[j].properties.healthy_veg;
                tot[j] = b[j] + u[j] + m[j] + h[j];
                pb[j] = (b[j]/tot[j])*100;
                pu[j] = (u[j]/tot[j])*100;
                pm[j] = (m[j]/tot[j])*100;
                ph[j] = (h[j]/tot[j])*100;
            }
            
            
            grid0.on("click",function(){
                mychart = $('#mychart');
                Chart.defaults.global.defaultFontColor ='black',
                chart_in = new Chart(mychart, {
                    type: 'bar',
                    data: {
                    labels:['Bare Soil', 'Unhealthy', 'Moderate', 'Healthy'],
                    datasets:[{
                        //label:'Percentage',
                        data:[pb[0], pu[0], pm[0], ph[0],],
                        backgroundColor:['brown','red','yellow','green'],
                        borderWidth:1,
                        borderColor:'grey',
                        hoverBorderWidth:4,
                        hoverBorderColor:'blue'
                    }]
                    },
                    options: chart_options
                })
            });   
            ///////////////

            grid1.on("click",function(){
                mychart = $('#mychart');
                Chart.defaults.global.defaultFontColor ='black',
                chart_in = new Chart(mychart, {
                    type: 'bar',
                    data: {
                    labels:['Bare Soil', 'Unhealthy', 'Moderate', 'Healthy'],
                    datasets:[{
                        label:'Percentage',
                        data:[pb[1], pu[1], pm[1], ph[1],],
                        backgroundColor:['brown','red','yellow','green'],
                        borderWidth:1,
                        borderColor:'grey',
                        hoverBorderWidth:4,
                        hoverBorderColor:'blue'
                    }]
                    },
                    options: chart_options
                })
            });   
            ///////////////

            grid2.on("click",function(){
                mychart = $('#mychart');
                Chart.defaults.global.defaultFontColor ='black',
                chart_in = new Chart(mychart, {
                    type: 'bar',
                    data: {
                    labels:['Bare Soil', 'Unhealthy', 'Moderate', 'Healthy'],
                    datasets:[{
                        label:'Percentage',
                        data:[pb[2], pu[2], pm[2], ph[2],],
                        backgroundColor:['brown','red','yellow','green'],
                        borderWidth:1,
                        borderColor:'grey',
                        hoverBorderWidth:4,
                        hoverBorderColor:'blue'
                    }]
                    },
                    options: chart_options
                })
            });   
            ///////////////

            grid3.on("click",function(){
                mychart = $('#mychart');
                Chart.defaults.global.defaultFontColor ='black',
                chart_in = new Chart(mychart, {
                    type: 'bar',
                    data: {
                    labels:['Bare Soil', 'Unhealthy', 'Moderate', 'Healthy'],
                    datasets:[{
                        label:'Percentage',
                        data:[pb[3], pu[3], pm[3], ph[3],],
                        backgroundColor:['brown','red','yellow','green'],
                        borderWidth:1,
                        borderColor:'grey',
                        hoverBorderWidth:4,
                        hoverBorderColor:'blue'
                    }]
                    },
                    options: chart_options
                })
            });   
            ///////////////

            grid4.on("click",function(){
                mychart = $('#mychart');
                Chart.defaults.global.defaultFontColor ='black',
                chart_in = new Chart(mychart, {
                    type: 'bar',
                    data: {
                    labels:['Bare Soil', 'Unhealthy', 'Moderate', 'Healthy'],
                    datasets:[{
                        label:'Percentage',
                        data:[pb[0], pu[4], pm[4], ph[4],],
                        backgroundColor:['brown','red','yellow','green'],
                        borderWidth:1,
                        borderColor:'grey',
                        hoverBorderWidth:4,
                        hoverBorderColor:'blue'
                    }]
                    },
                    options: chart_options
                })
            });   
            ///////////////

            grid5.on("click",function(){
                mychart = $('#mychart');
                Chart.defaults.global.defaultFontColor ='black',
                chart_in = new Chart(mychart, {
                    type: 'bar',
                    data: {
                    labels:['Bare Soil', 'Unhealthy', 'Moderate', 'Healthy'],
                    datasets:[{
                        label:'Percentage',
                        data:[pb[5], pu[5], pm[5], ph[5],],
                        backgroundColor:['brown','red','yellow','green'],
                        borderWidth:1,
                        borderColor:'grey',
                        hoverBorderWidth:4,
                        hoverBorderColor:'blue'
                    }]
                    },
                    options: chart_options
                })
            });   
            ///////////////

            grid6.on("click",function(){
                mychart = $('#mychart');
                Chart.defaults.global.defaultFontColor ='black',
                chart_in = new Chart(mychart, {
                    type: 'bar',
                    data: {
                    labels:['Bare Soil', 'Unhealthy', 'Moderate', 'Healthy'],
                    datasets:[{
                        label:'Percentage',
                        data:[pb[6], pu[6], pm[6], ph[6],],
                        backgroundColor:['brown','red','yellow','green'],
                        borderWidth:1,
                        borderColor:'grey',
                        hoverBorderWidth:4,
                        hoverBorderColor:'blue'
                    }]
                    },
                    options: chart_options
                })
            });   
            ///////////////

            grid7.on("click",function(){
                mychart = $('#mychart');
                Chart.defaults.global.defaultFontColor ='black',
                chart_in = new Chart(mychart, {
                    type: 'bar',
                    data: {
                    labels:['Bare Soil', 'Unhealthy', 'Moderate', 'Healthy'],
                    datasets:[{
                        label:'Percentage',
                        data:[pb[7], pu[7], pm[7], ph[7],],
                        backgroundColor:['brown','red','yellow','green'],
                        borderWidth:1,
                        borderColor:'grey',
                        hoverBorderWidth:4,
                        hoverBorderColor:'blue'
                    }]
                    },
                    options: chart_options
                })
            });   
            ///////////////

            grid8.on("click",function(){
                mychart = $('#mychart');
                Chart.defaults.global.defaultFontColor ='black',
                chart_in = new Chart(mychart, {
                    type: 'bar',
                    data: {
                    labels:['Bare Soil', 'Unhealthy', 'Moderate', 'Healthy'],
                    datasets:[{
                        label:'Percentage',
                        data:[pb[8], pu[8], pm[8], ph[8],],
                        backgroundColor:['brown','red','yellow','green'],
                        borderWidth:1,
                        borderColor:'grey',
                        hoverBorderWidth:4,
                        hoverBorderColor:'blue'
                    }]
                    },
                    options: chart_options
                })
            });   
            ///////////////
                     
        
        
               
        });
